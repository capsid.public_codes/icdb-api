import psycopg2
from .storage import DataModelStorage
from .vocabulary import CommonVocabulary
import json

class PostgresDataModelStorage(DataModelStorage):
    """
    Implementation of a Postgres storage backend of the patient data model.
    """

    _table_study = "study"
    _table_subject = "subject"
    _table_timepoint = "timepoint"
    _table_datapoint = "datapoint"
    _table_vocabulary = "vocabulary"
    _table_dataset = "dataset"
    _table_experiment = "experiment"
    _table_pattern = "pattern"
    _table_cluster = "cluster"
    _table_rule = "rule"
    _table_decision_tree = "decision_tree"
    _table_statistical_test = "statistical_test"

    _json_entity_table_definition = ('uri TEXT PRIMARY KEY NOT NULL', 'json JSONB NOT NULL')

    _table_definitions = [
        (_table_study,) + _json_entity_table_definition,
        (_table_subject, 'uri TEXT PRIMARY KEY NOT NULL'),
        (_table_timepoint,) + _json_entity_table_definition,
        (_table_datapoint,) + _json_entity_table_definition,
        (_table_vocabulary,) + _json_entity_table_definition,
        (_table_dataset,) + _json_entity_table_definition,
        (_table_experiment,) + _json_entity_table_definition,
        (_table_pattern,) + _json_entity_table_definition,
        (_table_cluster,) + _json_entity_table_definition,
        (_table_rule,) + _json_entity_table_definition,
        (_table_decision_tree,) + _json_entity_table_definition,
        (_table_statistical_test,) + _json_entity_table_definition,
    ]

    _index_definitions = [
        "CREATE INDEX IF NOT EXISTS data_uri ON %s ((json->>'uri'))" % (_table_datapoint),
        "CREATE INDEX IF NOT EXISTS data_name ON %s ((json->>'name'))" % (_table_datapoint),
        "CREATE INDEX IF NOT EXISTS data_variable ON %s ((json->>'variable'))" % (_table_datapoint),
        "CREATE INDEX IF NOT EXISTS data_study ON %s ((json->>'study'))" % (_table_datapoint),
        "CREATE INDEX IF NOT EXISTS data_subject ON %s ((json->>'subject'))" % (_table_datapoint),
        "CREATE INDEX IF NOT EXISTS data_type ON %s ((json->>'type'))" % (_table_datapoint),
        "CREATE INDEX IF NOT EXISTS data_timepoint ON %s ((json->>'timepoint'))" % (_table_datapoint),

        "CREATE INDEX IF NOT EXISTS subject_uri ON %s (uri)" % (_table_subject),
        "CREATE INDEX IF NOT EXISTS timepoint_uri ON %s (uri)" % (_table_timepoint),
        "CREATE INDEX IF NOT EXISTS study_uri ON %s (uri)" % (_table_study),
        "CREATE INDEX IF NOT EXISTS vocabulary_uri ON %s (uri)" % (_table_vocabulary),

        "CREATE INDEX IF NOT EXISTS timepoint_json_uri ON %s ((json->>'uri'))" % (_table_timepoint),
        "CREATE INDEX IF NOT EXISTS study_json_uri ON %s ((json->>'uri'))" % (_table_study),
        "CREATE INDEX IF NOT EXISTS vocabulary_json_uri ON %s ((json->>'uri'))" % (_table_vocabulary),
    ]

    def __init__(self):
        self.db = None
        self.cur = None
        self.nr_inserts = 0
        self.commit_nr = 1e4

    def _store_json_entity(self, entity, table, conflict_action='fail', has_data=True):
        self._check_connected()

        json = entity.to_json()

        if has_data:
            query = 'INSERT INTO ' + table + ' (uri, json) VALUES (%s, %s)'
            values = (entity.uri, json)
            if conflict_action is 'overwrite':
                query = '%s ON CONFLICT DO UPDATE SET json = %s' % query
                values += (json,)
            elif conflict_action is 'ignore':
                query = '%s ON CONFLICT DO NOTHING' % query
        else:
            query = 'INSERT INTO ' + table + ' (uri) VALUES (%s)'
            values = (entity.uri,)
            if conflict_action is 'overwrite' or conflict_action is 'ignore':
                query = '%s ON CONFLICT DO NOTHING' % query

        self._do_insert(query, values)

    def store_data_point(self, data_point, conflict_action='fail'):
        self._store_json_entity(data_point, PostgresDataModelStorage._table_datapoint, conflict_action)

    def store_subject(self, subject, conflict_action='fail'):
        self._store_json_entity(subject, PostgresDataModelStorage._table_subject, conflict_action, False)

    def store_study(self, study, conflict_action='fail'):
        self._store_json_entity(study, PostgresDataModelStorage._table_study, conflict_action)

    def store_timepoint(self, timepoint, conflict_action='fail'):
        self._store_json_entity(timepoint, PostgresDataModelStorage._table_timepoint, conflict_action)

    def store_dataset(self, dataset, conflict_action='fail'):
        self._store_json_entity(dataset, PostgresDataModelStorage._table_dataset, conflict_action)

    def store_experiment(self, experiment, conflict_action='fail'):
        self._store_json_entity(experiment, PostgresDataModelStorage._table_experiment, conflict_action)

    def store_pattern(self, pattern, conflict_action='fail'):
        self._store_json_entity(pattern, PostgresDataModelStorage._table_pattern, conflict_action)

    def store_cluster(self, cluster, conflict_action='fail'):
        self._store_json_entity(cluster, PostgresDataModelStorage._table_cluster, conflict_action)

    def store_rule(self, rule, conflict_action='fail'):
        self._store_json_entity(rule, PostgresDataModelStorage._table_rule, conflict_action)

    def store_decision_tree(self, decision_tree, conflict_action='fail'):
        self._store_json_entity(decision_tree, PostgresDataModelStorage._table_decision_tree, conflict_action)

    def store_statistical_test(self, statistical_test, conflict_action='fail'):
        self._store_json_entity(statistical_test, PostgresDataModelStorage._table_statistical_test, conflict_action)

    def store_common_vocabulary(self, vocabulary, conflict_action='fail'):
        if conflict_action == 'overwrite':
            self._clear_table(PostgresDataModelStorage._table_vocabulary)

        for e in vocabulary.get_definitions().values():
            self._store_json_entity(e, PostgresDataModelStorage._table_vocabulary, conflict_action)

    def get_common_vocabulary(self):
        definitions = {}
        with self.db:
            with self.db.cursor() as cur:
                cur.execute("SELECT uri,json FROM %s" % PostgresDataModelStorage._table_vocabulary)
                for r in cur:
                    definitions[r[0]] = json.loads(r[1])
        vocabulary = CommonVocabulary()
        vocabulary.set_definitions(definitions)
        return vocabulary

    def flush(self):
        if self.db:
            self.db.commit()
            self._reset_cursor()

    def open(self, connection_string):
        self.close()

        self.db = psycopg2.connect(connection_string)
        self._init_database()

    def close(self):
        self.flush()
        if self.db:
            self.db.close()

    def clear_database(self):
        """
        Clear the database containing the patient data model and reset the table structure.
        """
        self._check_connected()

        #with self.db:
        with self.db.cursor() as cur:
            for tbl in PostgresDataModelStorage._table_definitions:
                cur.execute("DROP TABLE IF EXISTS %s CASCADE" % tbl[0])
                self._init_database()

    def _reset_cursor(self):
        if self.cur:
            self.cur.close()
            self.cur = self.db.cursor()
            self.nr_inserts = 0

    def _do_insert(self, query, values):
        if not self.cur:
            self.cur = self.db.cursor()

        if self.nr_inserts >= self.commit_nr:
            self.db.commit()
            self._reset_cursor()

        try:
            self.cur.execute(query, values)
            self.nr_inserts += 1
        except Exception as ex:
            self.db.rollback()
            self._reset_cursor()
            raise ex

    def _check_connected(self):
        if not self.db:
            raise Exception('Not connected to a database, please call connect_database first!')

    def _clear_table(self, table):
        self._check_connected()

        with self.db:
            with self.db.cursor() as cur:
                cur.execute("TRUNCATE %s" % table)

    def _init_database(self):
        self._check_connected()

        with self.db:
            with self.db.cursor() as cur:
                cur.execute("CREATE EXTENSION IF NOT EXISTS pgcrypto")
                cur.execute("CREATE EXTENSION IF NOT EXISTS tablefunc")
                for tbl in PostgresDataModelStorage._table_definitions:
                    sql = ','.join(tbl[1:])
                    cur.execute("CREATE TABLE IF NOT EXISTS %s (%s);" % (tbl[0], sql))

                for index in PostgresDataModelStorage._index_definitions:
                    cur.execute(index)
