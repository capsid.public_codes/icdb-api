import abc

class DataModelStorage:
    """
    Abstract class for a storage backend of the patient data model.

    Allows for implementing different storage backends to store the data model.
    """

    @abc.abstractmethod
    def store_data_point(self, data_point, conflict_action='fail'):
        """
        Store a data point.

        Args:
            data_point: The data point (instance of model.DataPoint).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """

    def store_measurement(self, measurement, conflict_action='fail'):
        """
        Store a measurement.

        Args:
            measurement: The measurement (instance of model.Measurement).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """
        self.store_data_point(measurement, conflict_action)

    def store_feature(self, feature, conflict_action='fail'):
        """
        Store a feature.

        Args:
            feature: The feature (instance of model.Feature).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """
        self.store_data_point(feature, conflict_action)

    def store_event(self, event, conflict_action='fail'):
        """
        Store an event.

        Args:
            event: The event (instance of model.Event).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """
        self.store_data_point(event, conflict_action)

    @abc.abstractmethod
    def store_subject(self, subject):
        """
        Store a subject.

         Subjects are represented solely by their uri,
         all other information about the subject should be stored through features,
         measurements, and events.
         Subjects are unique, if the subject already exists, no new information will be
         stored and the subject uri can readily be used to store data points.

        Args:
            subject: The subject (instance of model.Subject).
        """

    @abc.abstractmethod
    def store_study(self, study, conflict_action='fail'):
        """
        Store a study.

        Args:
            study: The study (instance of model.Study).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """

    @abc.abstractmethod
    def store_timepoint(self, timepoint, conflict_action='fail'):
        """
        Store a timepoint.

        Args:
            timepoint: The timepoint (instance of model.Timepoint).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """

    @abc.abstractmethod
    def store_common_vocabulary(self, vocabulary, conflict_action='fail'):
        """
        Store the common vocabulary.

        Args:
            vocabulary: The vocabulary (instance of vocabulary.CommonVocabulary).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """

    @abc.abstractmethod
    def store_dataset(self, dataset, conflict_action='fail'):
        """
        Store a dataset.

        Args:
            dataset: The dataset (instance of model.Dataset).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """

    @abc.abstractmethod
    def store_experiment(self, experiment, conflict_action='fail'):
        """
        Store an experiment.

        Args:
            experiment: The experiment (instance of model.Experiment).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """

    @abc.abstractmethod
    def store_pattern(self, pattern, conflict_action='fail'):
        """
        Store a pattern.

        Args:
            pattern: The pattern (instance of model.Pattern).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """

    @abc.abstractmethod
    def store_cluster(self, cluster, conflict_action='fail'):
        """
        Store a study.

        Args:
            cluster: The cluster (instance of model.Cluster).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """

    @abc.abstractmethod
    def store_rule(self, rule, conflict_action='fail'):
        """
        Store a rule.

        Args:
            rule: The rule (instance of model.Rule).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """

    @abc.abstractmethod
    def store_decision_tree(self, decision_tree, conflict_action='fail'):
        """
        Store a decision_tree.

        Args:
            study: The decision_tree (instance of model.Decision_tree).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """

    @abc.abstractmethod
    def store_statistical_test(self, statistical_test, conflict_action='fail'):
        """
        Store a statistical_test.

        Args:
            statistical_test: The statistical_test (instance of model.Statistical_test).
            conflict_action: What to do if an item with the same uri already exists ('fail' to throw an error, 'overwrite' to overwrite existing data, 'ignore' to keep the orignial data).
        """

    @abc.abstractmethod
    def get_common_vocabulary(self):
        """
        Get the common vocabulary as stored in the data model storage.

        Returns an instance of vocabulary.CommonVocabulary or None if no vocabulary
        is present in the data model storage.
        """

    @abc.abstractmethod
    def close(self):
        """
        Close the currently open connection to the patient data model storage instance.

        All pending transactions will be committed prior to closing the connection.
        """

    @abc.abstractmethod
    def open(self, connection_string):
        """
        Open a connection to a patient data model storage instance.

        The structure of the connection string depends on the storage implementation.
        """

    @abc.abstractmethod
    def flush(self):
        """
        Commit all pending transactions.
        """
