CREATE TABLE IF NOT EXISTS cluster (
    uri text NOT NULL,
    json jsonb NOT NULL,
    CONSTRAINT cluster_pkey PRIMARY KEY (uri)
);

CREATE TABLE IF NOT EXISTS datapoint (
    uri text NOT NULL,
    json jsonb NOT NULL,
    CONSTRAINT datapoint_pkey PRIMARY KEY (uri)
);

CREATE TABLE IF NOT EXISTS dataset (
    uri text NOT NULL,
    json jsonb NOT NULL,
    CONSTRAINT dataset_pkey PRIMARY KEY (uri)
);

CREATE TABLE IF NOT EXISTS decision_tree (
    uri text NOT NULL,
    json jsonb NOT NULL,
    CONSTRAINT decision_tree_pkey PRIMARY KEY (uri)
);

CREATE TABLE IF NOT EXISTS experiment (
    uri text NOT NULL,
    json jsonb NOT NULL,
    CONSTRAINT experiment_pkey PRIMARY KEY (uri)
);

CREATE TABLE IF NOT EXISTS pattern (
    uri text NOT NULL,
    json jsonb NOT NULL,
    CONSTRAINT pattern_pkey PRIMARY KEY (uri)
);

CREATE TABLE IF NOT EXISTS rule (
    uri text NOT NULL,
    json jsonb NOT NULL,
    CONSTRAINT rule_pkey PRIMARY KEY (uri)
);

CREATE TABLE IF NOT EXISTS statistical_test (
    uri text NOT NULL,
    json jsonb NOT NULL,
    CONSTRAINT statistical_test_pkey PRIMARY KEY (uri)
);

CREATE TABLE IF NOT EXISTS study (
    uri text NOT NULL,
    json jsonb NOT NULL,
    CONSTRAINT study_pkey PRIMARY KEY (uri)
);

CREATE TABLE IF NOT EXISTS subject (
    uri text NOT NULL,
    CONSTRAINT subject_pkey PRIMARY KEY (uri)
);

CREATE TABLE IF NOT EXISTS timepoint (
    uri text NOT NULL,
    json jsonb NOT NULL,
    CONSTRAINT timepoint_pkey PRIMARY KEY (uri)
);

CREATE TABLE IF NOT EXISTS vocabulary (
    uri text NOT NULL,
    json jsonb NOT NULL,
    CONSTRAINT vocabulary_pkey PRIMARY KEY (uri)
)