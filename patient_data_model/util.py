"""
This module contains utility classes and functions.
"""

import simplejson
import datetime

xsd_to_sql_types = {
    'xsd:int': 'int',
    'xsd:integer': 'int',
    'xsd:decimal': 'real',
    'xsd:boolean': 'boolean',
    'xsd:string': 'text',
    '': 'text',
    None: 'text'
}

def create_view_for_datapoints(conn, view_name='integrated_datapoint_table', use_variable_uris=True):
    with conn:
        with conn.cursor() as cur:
            cur.execute("""
            SELECT DISTINCT
            vocabulary.uri as variable,
            vocabulary.json->>'name' as name,
            vocabulary.json->>'xsd_type' as xsd_type
            FROM vocabulary
            INNER JOIN datapoint ON (datapoint.json->>'variable' = vocabulary.uri)
            """)
            variables = cur.fetchall()

    values_part = 'VALUES ' + ', '.join(["(''" + uri + "'')" for uri,name,type in variables])
    columns_part = ', '.join(
        ['"' + (uri if use_variable_uris else name) + '" ' + xsd_to_sql_types[type] for uri,name,type in variables]
    )

    remove_query = "DROP MATERIALIZED VIEW IF EXISTS %s" % view_name
    query = """
    CREATE MATERIALIZED VIEW %s AS SELECT * FROM crosstab(
        'SELECT
            CONCAT(datapoint.json->>''subject'', datapoint.json->>''study'', timepoint.json->>''date'', timepoint.json->>''label'') AS row_name,
            datapoint.json->>''subject'' AS subject,
            datapoint.json->>''study'' AS study,
            study.json->>''name'' AS study_name,
            datapoint.json->>''timepoint'' AS timepoint,
            timepoint.json->>''date'' AS timepoint_date,
            timepoint.json->>''label'' AS timepoint_label,
            datapoint.json->>''variable'' AS variable,
            datapoint.json->>''value'' AS value
        FROM datapoint, timepoint, study
            WHERE
                datapoint.json->>''timepoint'' = timepoint.uri AND
                datapoint.json->>''study'' = study.uri
        ORDER BY 1
        ',
        '%s'
    ) AS value(
        row_name text, subject text, study text, study_name text, timepoint text, timepoint_date text, timepoint_label text, %s
    )
    """ % (view_name, values_part, columns_part)

    with conn:
        with conn.cursor() as cur:
            cur.execute(remove_query)
            cur.execute(query)


class JsonDataEntity(object):
    """
    Utility class to provide an entity with an uri and data object that can be serialized to json.

    For the data object, required fields
    can be specified and validated upon serialization to json.
    """

    def __init__(self, uri, required_fields=[]):
        self.uri = uri
        self._data = {'uri': uri}
        self._required_fields = required_fields

    def add_required_fields(self, fields):
        self._required_fields += fields

    def set_data_field(self, key, value):
        """
        Set a field + value in the data object for this entity.
        """
        if isinstance(value, datetime.datetime):
            value = value.isoformat()

        self._data[key] = value

    def get_data_field(self, key):
        """
        Get the value for a field in the data object for this entity.
        """
        return self._data[key]

    def get_data(self):
        return self._data

    def validate(self):
        """
        Validate this entity by:

        * Checking if all required fields are present
        """
        for f in self._required_fields:
            if f not in self._data:
                raise Exception("Required field %s is missing from data.", f)

    def to_json(self):
        """
        Export the data object to a json string.
        """
        self.validate()
        return simplejson.dumps(self._data, ignore_nan=True)
