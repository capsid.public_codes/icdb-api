FROM python:3.6.5

ARG uid
ARG gid

RUN groupadd -g ${gid} jupyter || true
RUN useradd -u ${uid} -g ${gid} -m jupyter || true

ENV TINI_VERSION v0.6.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini

RUN pip install --upgrade pip

COPY requirements.txt ./
RUN pip install -r ./requirements.txt
RUN pip install sklearn

USER jupyter

COPY patient_data_model-*.whl /tmp/
RUN pip install --upgrade --user /tmp/patient_data_model-*.whl

CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--NotebookApp.token=''", "--ip=0.0.0.0", "--notebook-dir=/notebooks"]
ENTRYPOINT ["/usr/bin/tini", "--"]
