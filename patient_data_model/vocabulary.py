"""
This module contains classes and functions that deal with the common vocabulary of the patient data model.
"""
from .util import JsonDataEntity
import simplejson

class CommonVocabulary(object):
    """
    Common vocabulary for the patient data model.

    A collection of variable definitions (VariableDefinition instances), each
    identified by a URI.
    """

    def __init__(self):
        self.definitions = {}

    def set_definitions(self, definitions):
        """
        Set the variable definitions for this vocabulary.

        Args:
            definitions: A dict with URIs as keys and instances of VariableDefinition as values.
        """
        self.definitions = definitions

    def add_definition(self, definition):
        """
        Add a variable definition to this vocabulary.

        Args:
            definition: an instance of VariableDefinition.
        """
        self.definitions[definition.uri] = definition

    def get_definitions(self):
        """
        Get all variable definitions for this vocabulary.
        """
        return self.definitions

    def get_definition(self, uri):
        """
        Get the variable definition for the variable identified by the provided URI.

        If no definition exists for the URI, None will be returned.
        """
        return self.definitions.get(uri, None)

    def to_dict(self):
        """
        Export the vocabulary to a dict.
        """
        for d in self.definitions.values():
            d.validate()

        return {k: v.get_data() for k, v in self.definitions.items()}

    def to_json(self):
        """
        Export the vocabulary to json.
        """
        return simplejson.dumps(self.to_dict(), ignore_nan=True)

class VariableDefinition(JsonDataEntity):
    """
    Definition of a variable in the CommonVocabulary.

    This class contains variable specifications, such as uri, name, description, allowed data types.
    """

    def __init__(self, uri, name, description, xsd_type='xsd:string'):
        """
        Use VariableDefinition(uri, name, description, xsd_type) to create a new VariableDefinition instance.

        Args:
            uri: The URI for the variable definition.
            name: Human readable name for the variable.
            description: Human readable description of the variable.
            xsd_type: The xsd type of the variable, see https://www.w3.org/TR/xmlschema-2/#built-in-primitive-datatypes
        """
        super().__init__(uri, ['uri', 'name', 'description', 'xsd_type'])
        self.set_data_field('name', name)
        self.set_data_field('description', description)
        self.set_data_field('xsd_type', xsd_type)

    def set_allowed_units(self, units):
        """
        Set the allowed units for this variable.

        Args:
            units: A list of the allowed units (e.g. ['kg', 'g'])
        """
        self.set_data_field('allowed_units', units)

    def get_allowed_units(self):
        self.get_data_field('allowed_units')

    def set_allowed_values(self, enum):
        """
        Set the allowed values for this variable (effectively makes it an enumeration).

        Args:
            enum: A list of the allowed values (e.g. ["male", "female"])
        """
        self.set_data_field('allowed_values', enum)

    def get_allowed_values(self):
        self.get_data_field('allowed_values')

    def set_mappings(self, mappings):
        """
        Specify URIs to external entities (e.g. proteins, diseases) to which this variable maps.

        This can be used to link to related entities, e.g. "History of diabetes" would map to the
        disease ontology identifier for diabetes.
        """
        self.set_data_field('maps_to', mappings)

    def get_mappings(self):
        self.get_data_field('maps_to')
