## Patient Data Model
This repository contains FIGHT-HF patient data model combined with the inductive database API.

### Installation
The data model can be used in Python by installing the patient_data_model package from the *patient_data_model-x.x.x-py2.py3-none-any.whl* file.

#### Requirements
##### API
  * python 3.6
  * psycopg2 2.9.3
  * simplejson 3.17.6

##### DBMS
  * PostgreSQL 9.6


### Running the examples
A docker composition has been prepared that gives you a ready-to-go environment to play with.
An [example notebook](/examples/patient_data_model_example.ipynb) is provided that illustrates the use of the patient data model on an example dataset.

#### Requirements
  * git
  * docker
  * docker-compose
  * direnv

To run the examples yourself:
 * Clone this repository
 * Allow direnv to use the .envrc file
```sh
direnv allow
```

 * Start the docker containers with the following command:

```sh
docker-compose up -d
```

 * Then navigate to http://localhost:8888 to run the notebook.
 * A PGAdmin interface is also available at http://localhost:5050 (user: pgadmin4@pgadmin.org, pwd: admin and database pwd: test)
 * Use the following command to stop the docker containers:
```sh
docker-compose stop
```
