"""
This module contains classes and functions that represent data part of the patient data model.
"""
from .util import JsonDataEntity
from .vocabulary import VariableDefinition

class Study(JsonDataEntity):
    """
    The Study class defines a single study (e.g. cohort) to which data points can be linked.

    Use the 'set_*' methods to set the required fields.

    The required fields for Study are:
    name: The name of the study
    """

    def __init__(self, uri, name):
        """
        Use Study(uri) to create a new study instance.

        Args:
            uri: The URI for the study.
        """
        super().__init__(uri, ['name'])
        self.set_data_field('name', name)

    def set_sources(self, sources):
        """
        Set the original sources from which the study data originated.

        Args:
            sources: The sources, specified as a list of uris (e.g.
            ['file:///data/file1.xlsx', 'file:///data/file2.xlsx'])
        """
        self.set_data_field('sources', sources)

class Subject(JsonDataEntity):
    """
    The subject class defines a single subject (e.g. patient) to which data points can be linked.
    """

    def __init__(self, uri):
        """
        Use Subject(uri) to create a new study instance.

        Args:
            uri: The URI for the subject.
        """
        super().__init__(uri)

class Timepoint(JsonDataEntity):
    """
    The timepoint class defines a timepoint to which measurements and events can be linked.

    Use the 'set_*' methods to set the required fields.

    A timepoint requires at least one of the following fields:
    date: The absolute date for the timepoint (use a Python date object)
    label: The label for the timepoint (e.g. "visit 1" or "6 months after inclusion")
    offset: An offset time relative to the inclusion/first timepoint (e.g. 6)
    offset_unit: The unit for the offset (e.g. "months")
    """

    def __init__(self, uri):
        """
        Use Timepoint(uri) to create a new study instance.

        Args:
            uri: The URI for the timepoint.
        """
        super().__init__(uri)

    def set_label(self, label):
        """
        Set the label of the timepoint (e.g. "visit 1").
        """
        self.set_data_field('label', label)

    def set_date(self, date):
        """
        Set the absolute date of the timepoint (use a python date object).
        """
        self.set_data_field('date', date)

    def set_offset(self, offset):
        """
        Set the offset for the timepoint (for timepoints relative to the first/inclusion timepoint).

        Use the 'offset_unit' field to specify the units of the offset.
        """
        self.set_data_field('offset', offset)

    def set_offset_unit(self, unit):
        """
        Set the unit for the offset of the timepoint (specified in the 'offset' field).
        """
        self.set_data_field('offset_unit', unit)

    def _validate(self):
        super._validate()
        # Make sure we have either date or label
        if 'date' not in self._data and 'label' not in self._data:
            raise Exception("At least one of fields date or label should be provided.")

class DataPoint(JsonDataEntity):
    """
    The DataPoint class defines a data point in a cohort dataset.

    A data point has a type attribute, indicating the semantic meaning of the data point
    (e.g whether it is a measurement, event, or static feature).

    Default types are provided as classes extending DataPoint.

    Custom types can be added by creating an instance of DataPoint and providing
    the type as string in the constructor, or by extending this class.

    A datapoint is linked to a specific study, subject, and (optionally) timepoint, through URIs.

    The actual data needs to be provided by calling the "set_value" method. Optinally,
    the variable definition and unit can be provided using the "set_variable_definition" and
    "set_unit" methods.
    """

    def __init__(self, uri, type, variable, study_uri, subject_uri, timepoint_uri=None):
        """
        Use DataPoint(uri, type, variable, study_uri, subject_uri, timepoint_uri) to create a new instance.

        Args:
            uri: The URI for the datapoint.
            type: The semantic type of the datapoint (e.g. feature, measurement, event).
            variable: The variable this datapoint contains a value of (can be a uri, or an instance of vocabulary.VariableDefinition)
            study_uri: The URI of the study this datapoint refers to.
            subject_uri: The URI of the subject this datapoint refers to.
            timepoint_uri: The URI of the timepoint this datapoint refers to (optional).
        """
        super().__init__(uri, ['type', 'variable', 'study', 'subject'])
        self.set_data_field('type', type)
        self.set_data_field('study', study_uri)
        self.set_data_field('subject', subject_uri)
        if timepoint_uri is not None:
            self.set_data_field('timepoint', timepoint_uri)

        if isinstance(variable, VariableDefinition):
            self.set_variable_definition(variable)
        else:
            self.set_data_field('variable', variable)

    def set_variable_definition(self, variable_definition):
        """
        Set the variable definition for this datapoint.

        The variable definition links the datapoint to an entity in the
        common vocabulary and allows for integration, variable conversion, and validation.

        Args:
            variable_definition: The variable definition (an instance of vocabulary.VariableDefinition)
        """
        self.variable_definition = variable_definition
        self.set_data_field('variable', variable_definition.get_data_field('uri'))

    def set_value(self, value):
        """
        Set the data value for this datapoint.
        """
        self.set_data_field('value', value)

    def set_unit(self, unit):
        """
        Set the unit for this datapoint.

        If the variable definition is specified, it should be one of
        the values returned by VariableDefinition.get_allowed_units
        """
        self.set_data_field('unit', unit)

    def set_sources(self, sources):
        """
        Set the original sources from which the datapoint originated.

        Args:
            sources: The sources, specified as a list of uris (e.g.
            ['file:///data/file1.xlsx#row1#colA', 'file:///data/file2.xlsx#row2#colB'])
        """
        self.set_data_field('sources', sources)

class Feature(DataPoint):
    """
    The feature class defines a data point of the type "feature".

    Features are (typically static) properties of a subject within a study, such as birth date or sex.
    A feature is linked to a specific study and subject through URIs.
    A feature may have a timepoint (e.g. to indicate the point when the feature was recorded). In contrast
    to a measurement and event, the timepoint property is optional for a feature.
    """

    def __init__(self, uri, variable, study_uri, subject_uri, timepoint_uri=None):
        """
        Use Feature(uri, variable, study_uri, subject_uri) to create a new instance.

        Args:
            uri: The URI for the datapoint.
            variable: The variable this datapoint contains a value of (can be a uri, or an instance of vocabulary.VariableDefinition)
            study_uri: The URI of the study this datapoint refers to.
            subject_uri: The URI of the subject this datapoint refers to.
            timepoint_uri: The URI of the timepoint this datapoint refers to (optional).
        """
        super().__init__(uri, 'feature', variable, study_uri, subject_uri, timepoint_uri)

class Measurement(DataPoint):
    """
    The measurement class defines a data point of type "measurement".

    Measurements are data points obtained at a specific timepoint within a study, such as
    biomarker measurements, or clinical variables.

    A measurement is linked to a specific study, subject, and timepoint through URIs.
    """

    def __init__(self, uri, variable, study_uri, subject_uri, timepoint_uri):
        """
        Use Measurement(uri, variable, study_uri, subject_uri, timepoint_uri) to create a new instance.

        Args:
            uri: The URI for the datapoint.
            variable: The variable this datapoint contains a value of (can be a uri, or an instance of vocabulary.VariableDefinition)
            study_uri: The URI of the study this datapoint refers to.
            subject_uri: The URI of the subject this datapoint refers to.
            timepoint_uri: The URI of the timepoint this datapoint refers to.
        """
        super().__init__(uri, 'measurement', variable, study_uri, subject_uri, timepoint_uri)
        self.add_required_fields(['timepoint'])

class Event(DataPoint):
    """
    The Event class defines a data point of type "event".

    Events represent an event that occurred to a subject within a study at a specific timepoint,
    such as an intervention, surgery, or death.

    An event is linked to a specific study, subject, and timepoint through URIs.
    """

    def __init__(self, uri, variable, study_uri, subject_uri, timepoint_uri):
        """
        Use Event(uri, variable, study_uri, subject_uri, timepoint_uri) to create a new instance.

        Args:
            uri: The URI for the datapoint.
            variable: The variable this datapoint contains a value of (can be a uri, or an instance of vocabulary.VariableDefinition)
            study_uri: The URI of the study this datapoint refers to.
            subject_uri: The URI of the subject this datapoint refers to.
            timepoint_uri: The URI of the timepoint this datapoint refers to.
        """
        super().__init__(uri, 'event', variable, study_uri, subject_uri, timepoint_uri)
        self.add_required_fields(['timepoint'])


class Dataset(JsonDataEntity):
    """
    
    """

    def __init__(self, uri, name, description, subjects, variables):
        """
        Use Dataset(uri, name, description, subjects, variables) to create a new dataset instance.

        Args:
            uri: The URI for the subject.
        """
        super().__init__(uri)
        self.set_data_field('name', name)
        self.set_data_field('description', description)
        self.set_data_field('subjects', subjects)
        self.set_data_field('variables', variables)

class Experiment(JsonDataEntity):
    """
    
    """

    def __init__(self, uri, dataset, program, parameters, metrics, date):
        """
        Use Experiment(uri, name, description, subjects, variables) to create a new experiment instance.

        Args:
            uri: The URI for the subject.
        """
        super().__init__(uri)
        self.set_data_field('dataset', dataset)
        self.set_data_field('program', program)
        self.set_data_field('parameters', parameters)
        self.set_data_field('metrics', metrics)
        self.set_data_field('date', date)


class Pattern(JsonDataEntity):
    """
    
    """

    def __init__(self, uri, pattern, support, experiment):
        """
        Use Pattern(uri, pattern, support, experiment) to create a new pattern instance.

        Args:
            uri: The URI for the subject.
        """
        super().__init__(uri)
        self.set_data_field('pattern', pattern)
        self.set_data_field('support', support)
        self.set_data_field('experiment', experiment)


class StatisticalTest(JsonDataEntity):
    """
    
    """

    def __init__(self, uri, case_dataset, control_dataset, fdr, odd_ratio, control_support):
        """
        Use StatisticalTest(uri, case_dataset, control_dataset, fdr, odd_ratio, control_support) to create a new statistical test instance.

        Args:
            uri: The URI for the subject.
        """
        super().__init__(uri)
        self.set_data_field('case_dataset', case_dataset)
        self.set_data_field('control_dataset', control_dataset)
        self.set_data_field('fdr', fdr)
        self.set_data_field('odd_ratio', odd_ratio)
        self.set_data_field('control_support', control_support)


class Cluster(JsonDataEntity):
    """
    
    """

    def __init__(self, uri, experiment, description, subjects):
        """
        Use Cluster(uri, experiment, description, subjects) to create a new cluster instance.

        Args:
            uri: The URI for the subject.
        """
        super().__init__(uri)
        self.set_data_field('experiment', experiment)
        self.set_data_field('description', description)
        self.set_data_field('subjects', subjects)


class Rule(JsonDataEntity):
    """
    
    """

    def __init__(self, uri, experiment, left_pattern, right_pattern):
        """
        Use Rule(uri, experiment, left_pattern, right_pattern) to create a new rule instance.

        Args:
            uri: The URI for the subject.
        """
        super().__init__(uri)
        self.set_data_field('experiment', experiment)
        self.set_data_field('left_pattern', left_pattern)
        self.set_data_field('right_pattern', right_pattern)


class Decision_tree(JsonDataEntity):
    """
    
    """

    def __init__(self, uri, experiment, rules):
        """
        Use Decision_tree(uri, experiment, rules) to create a new decision tree instance.

        Args:
            uri: The URI for the subject.
        """
        super().__init__(uri)
        self.set_data_field('experiment', experiment)
        self.set_data_field('rules', rules)
